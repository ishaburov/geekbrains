<? include 'simple_html_dom.php';
function getInstagramContent ($link){
    $html = file_get_html($link);
// Find json
    foreach($html->find('script',3)->find('text') as $element){
        echo $element;
    }

}?>

<html>
<head>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
</head>
<body>
<div class="insagramm">
    <div class="insagramm__widjet">
        <div class="insagramm__widjet_proffile-image"><img src=""></div>
        <div class="insagramm__widjet_title"><span></span></div>
        <div class="insagramm__widjet_biography"> <span></span></div>
        <div class="insagramm__widjet_followed">Подписчиков: <span></span></div>
        <div class="insagramm__widjet_follow">Подписки: <span></span></div>
        <div class="insagramm__widjet_full_name"><span></span></div>
    </div>
    <div class="insagramm__images">

    </div>
</div>
<script type="text/javascript">
    <? getInstagramContent("https://www.instagram.com/jdm.auto/" ); ?>
</script>


<script>
    $( function ( ){
        console.log(JSON.stringify(window._sharedData));

        var title = window._sharedData.entry_data.ProfilePage[0].graphql.user.username;
        var biography = window._sharedData.entry_data.ProfilePage[0].graphql.user.biography;
        var edge_followed_by = window._sharedData.entry_data.ProfilePage[0].graphql.user.edge_followed_by.count;
        var edge_follow = window._sharedData.entry_data.ProfilePage[0].graphql.user.edge_follow.count;
        var full_name = window._sharedData.entry_data.ProfilePage[0].graphql.user.full_name;
        var proffile_image = window._sharedData.entry_data.ProfilePage[0].graphql.user.profile_pic_url_hd;

        var thumb = window._sharedData.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges;
        $.each(thumb,function (i) {
            var resustImage = this.node.display_url;
            $('.insagramm__images').append('<img src='+ resustImage +'>');
        });
        $(".insagramm__widjet_proffile-image img").attr("src",proffile_image);

        $(".insagramm__widjet_title span").text(title);
        $(".insagramm__widjet_biography span").text(biography);
        $(".insagramm__widjet_followed span").text(edge_followed_by);
        $(".insagramm__widjet_follow span").text(edge_follow);
        $(".insagramm__widjet_full_name span").text(full_name);
    });

</script>
</body>
</html>