<?php
function drowItems ($counter){
	global $dbh;
	$sql = "SELECT * FROM `items` LIMIT :offset , :limit" ;
	$sth = $dbh->prepare($sql);
	$sth->bindValue(":offset", $counter * 8, PDO::PARAM_INT);
	$sth->bindValue(":limit", 8, PDO::PARAM_INT);
	$sth->execute();
	$items = $sth->fetchAll(PDO::FETCH_CLASS);
	if ($counter >= 0 && count($items) >= $counter){
		foreach ($items as $item){?>
		<div class="item-inner lazy">
					<div class="items__item">
						<div class="items__item_inner">
							<div class="items__item_button"><a href="#">Подробнее</a></div>
							<div class="items__item_button"><a href="#">Добавить в корзину</a></div>
						</div>
						<a href="#" class="items__item_image">
							<div class="items__item_image-mask"></div>
							<img src="../../web/imgs/<?=$item->image?>" alt="">
						</a>
					</div>
					<div class="items__text">
						<div><h4><?= $item->price?><span>$</span></h4></div>
						<div><h4><?= $item->title?></h4></div>
					</div>
		</div>
		<?}
	}else{return die();}
	return $counter;
}
?>
