<html>
<head>
	<meta charset="UTF-8">
	<title>shop</title>
	<link rel="stylesheet" href="../web/css/style.css">
</head>
<body>
	<div class="container">
		<div class="items__inner">
			<?= drowItems(0)?>
		</div>
		<div class="button" data-event="1">Показать больше</div>
	</div>
	<script
		src="https://code.jquery.com/jquery-2.2.4.js"
		integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
		crossorigin="anonymous">
	</script>
	<script>

		$('.button').on('click',function () {
			var counter = $(this).data('event');
			$(this).data('event',counter + 1);
			$.ajax({
				url: "../controller/ajax/ajax.php",
				type: "POST",
				dataType:"html",
				data: {
					"min": counter
				},
				success: function(html){
					console.log(this.data);
					$('.items__inner').append(html);
				}
			});

		});
	</script>
</body>
</html>